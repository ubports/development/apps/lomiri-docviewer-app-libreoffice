# LibreOfficeKit Libraries for Document Viewer App

This project builds a minimial variant of LibreOffice with the LibreOfficeKit
Libraries using [clickable][2] which are used by the [Document Viewer App][1].
It is separated from the Document Viewer App because it is resource intensive
to build.

LibreOffice can be built by running

    clickable build --clean --libs libreoffice --arch $ARCH

where `$ARCH` contains the target architecture, e.g. arm64. The build results
will be placed into `build/$ARCH_TRIPLET/libreoffice` where `$ARCH_TRIPLET`
contains the target triplet, e.g. aarch64-linux-gnu. This directory has to be
placed in the same directory inside the [Document Viewer App][1] directory
before building it with clickable.


[1]: https://gitlab.com/ubports/development/apps/lomiri-docviewer-app
[2]: https://clickable-ut.dev/
